# Tarea 1 - Curso de Bootsrap - Coursera

## Requisitos
- Nodejs

## Como usar
- Clonar este repositorio
```
git clone https://viejony@bitbucket.org/viejony/bootstrap_tarea_1.git
```
- Instalar los paquetes de Nodejs
```
npm install
```
- Ejecutar lite-server para visualizar la página web en el navegador
```
npm run dev
```